from typing import Pattern
import unittest

from pii_indexer.patterns import PATTERNS


def regex_matches(regex: Pattern[str], subject: str) -> bool:
    match = regex.match(subject)
    return match is not None and match[0] == subject


class TestRegex(unittest.TestCase):
    def test_email_accepts(self):
        for test in [
            "user@contoso.co",
            "domain.user@clients.contoso.co.uk",
            "extended.mailbox-user+box@gmail.com",
            "root@localhost",
        ]:
            with self.subTest(msg=test):
                assert regex_matches(PATTERNS["EMAIL"], test)

    def test_email_rejects(self):
        for test in [
            "user@contoso.co.",
            ":user@contoso.co",
            "username@site.gov<br/>",
        ]:
            with self.subTest(msg=test):
                assert not regex_matches(PATTERNS["EMAIL"], test)

    def test_handle_accepts(self):
        for test in [
            "@jack",
            "@botuser1234",
        ]:
            with self.subTest(msg=test):
                assert regex_matches(PATTERNS["HANDLE"], test)

    def test_handle_rejects(self):
        for test in [
            "@bad@username",
            "bad@username@",
        ]:
            with self.subTest(msg=test):
                assert not regex_matches(PATTERNS["HANDLE"], test)

    def test_ipv4_accepts(self):
        for test in [
            "192.168.2.1",
            "1.1.1.1",
            "208.67.222.222",
            "255.255.255.0",
            "255.255.255.255",
        ]:
            with self.subTest(msg=test):
                assert regex_matches(PATTERNS["IPv4"], test)

    def test_ipv4_rejects(self):
        for test in [
            "256.1.1.1",
            "1920.10.80.1",
            "123456789",
        ]:
            with self.subTest(msg=test):
                assert not regex_matches(PATTERNS["IPv4"], test)

    def test_phone_accepts(self):
        for test in [
            "1234567890",
            "(123) 456 7890",
            "(123) 4567890",
            "(123) 456-7890",
            "(123) 456 - 7890",
            "(123)456-7890",
            "123-456-7890",
            "123 - 456 - 7890",
            "123.456.7890",
        ]:
            with self.subTest(msg=test):
                assert regex_matches(PATTERNS["PHONE"], test)

    def test_phone_rejects(self):
        for test in [
            "1 2 3 4 5 6 7 8 9 0",
            "(123456) 7890",
            "123 (456) 7890",
            "123-46-57890",
            "123 . 456 . 7890",
        ]:
            with self.subTest(msg=test):
                assert not regex_matches(PATTERNS["PHONE"], test)

    def test_ssn_accepts(self):
        for test in [
            "111-22-3333",
            "111223333",
        ]:
            with self.subTest(msg=test):
                assert regex_matches(PATTERNS["SSN"], test)

    def test_ssn_rejects(self):
        for test in [
            "1 1 1 2 2 3 3 3 3",
            "1112-23-333",
            "1112233334",
            "111 - 22 - 3333",
            " 111 - 22 - 3333 ",
        ]:
            with self.subTest(msg=test):
                assert not regex_matches(PATTERNS["SSN"], test)


if __name__ == "__main__":
    unittest.main()
