import unittest

from pii_indexer.patterns import normalize


class TestRegex(unittest.TestCase):
    def test_normalize_email(self):
        for test, expected in [
            ("user@contoso.co", "user@contoso.co"),
            ("USER@contoso.co", "user@contoso.co"),
            (" do.user@contoso.CO.UK ", "do.user@contoso.co.uk"),
            (" do.user@contoso.co.uk ", "do.user@contoso.co.uk"),
        ]:
            with self.subTest(msg=test):
                assert normalize(test, "EMAIL") == expected

    def test_normalize_handle(self):
        for test, expected in [
            ("@username", "username"),
            (" @username ", "username"),
            ("@USERNAME", "username"),
            (" @USERNAME ", "username"),
        ]:
            with self.subTest(msg=test):
                assert normalize(test, "HANDLE") == expected

    def test_normalize_ipv4(self):
        for test, expected in [
            ("192.168.2.1 ", "192.168.2.1"),
            (" 192.168.2.1", "192.168.2.1"),
            ("192.168.2.1", "192.168.2.1"),
        ]:
            with self.subTest(msg=test):
                assert normalize(test, "IPv4") == expected

    def test_normalize_phone(self):
        for test, expected in [
            ("1234567890", "1234567890"),
            ("(123) 456 7890", "1234567890"),
            ("(123) 4567890", "1234567890"),
            ("(123) 456-7890", "1234567890"),
            ("(123) 456 - 7890", "1234567890"),
            ("(123)456-7890", "1234567890"),
            ("123-456-7890", "1234567890"),
            (" 123 - 456 - 7890 ", "1234567890"),
            ("123.456.7890", "1234567890"),
        ]:
            with self.subTest(msg=test):
                assert normalize(test, "PHONE") == expected

    def test_normalize_ssn(self):
        for test, expected in [
            ("111-22-3333", "111223333"),
            ("111 - 22 - 3333", "111223333"),
            (" 111 - 22 - 3333 ", "111223333"),
            ("111223333", "111223333"),
            (" 111223333 ", "111223333"),
        ]:
            with self.subTest(msg=test):
                assert normalize(test, "SSN") == expected
