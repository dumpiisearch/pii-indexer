# Adding a new PII type

1. Define a regular expression in `patterns.py`
1. Add this regular expression to the `PATTERNS` dictionary (also in `patterns.py`)
1. Add a case to `normalize()` if special characters are involved
1. Create two tests in `test_regex.py`: one for accepted patterns and one for rejected patterns
1. Create a test in `test_normalize.py` 
