import setuptools

setuptools.setup(
    name="pii_indexer",
    version="0.3.2",
    author="Andy Castille",
    author_email="andy@robiotic.net",
    description="Be able to search a data dump without storing personally-identifiable information",
    url="https://gitlab.com/dumpiisearch/pii-indexer",
    packages=["pii_indexer"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPLv3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    entry_points=dict(
        console_scripts=[
            "pii-indexer=pii_indexer:main",
            "pii-export=pii_indexer:export",
        ],
    ),
    install_requires=["textract"],
)
