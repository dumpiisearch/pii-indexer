from argparse import ArgumentParser
from sys import argv

from pii_indexer.database import Database
from pii_indexer.scanner import Scanner


def main():
    arg_parser = ArgumentParser()
    arg_parser.add_argument(
        "--database",
        "-d",
        default="database.sqlite",
        help="The name of the SQLite 3 file to save the output into",
        dest="database_file",
        required=False,
    )
    arg_parser.add_argument(
        nargs="+", help="A list of files to scan", dest="input_files",
    )

    args = arg_parser.parse_args(argv[1:])
    db = Database(args.database_file)
    scanner = Scanner(db)

    for file in args.input_files:
        scanner.scan(file)
        db.connection.commit()

    db.connection.close()


def export():
    arg_parser = ArgumentParser()
    arg_parser.add_argument(
        "--output",
        "-o",
        default="data.sql",
        help="The name of the output file",
        dest="data_file",
        required=False,
    )
    arg_parser.add_argument(
        nargs="+", help="A list of SQLite files to read", dest="input_files",
    )

    args = arg_parser.parse_args(argv[1:])
    with open(args.data_file, "w") as output:
        for file in args.input_files:
            print("Exporting " + file)
            db = Database(file)
            output.write("-- " + file + "\n")
            output.write(db.to_sql())
            db.connection.close()


if __name__ == "__main__":
    main()
