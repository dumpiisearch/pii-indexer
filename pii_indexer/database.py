import hashlib
import sqlite3


def hash_str(input_str: str) -> str:
    return hashlib.blake2s(input_str.encode()).hexdigest()


def escape_str(input_str: str) -> str:
    """Not worried about injection, since these are static files."""
    return input_str.replace("'", r"\'")


class Database:
    def __init__(self, file: str):
        self.connection: sqlite3.Connection = sqlite3.connect(file)
        self.cursor: sqlite3.Cursor = self.connection.cursor()

        self.cursor.executescript(
            """
            CREATE TABLE IF NOT EXISTS PII (
                class   CHAR(8)         NOT NULL,
                info    CHAR(128)       NOT NULL,
                pos     INTEGER         NOT NULL,
                file    VARCHAR(128)    NOT NULL
            );
            
            CREATE UNIQUE INDEX IF NOT EXISTS
            PII_index on PII (class, info, pos, file);
            """
        )

    def add_data(self, data_type: str, data: str, file: str, position: int):
        self.cursor.execute(
            "INSERT OR IGNORE INTO PII VALUES (?, ?, ?, ?)",
            (data_type, hash_str(data), position, file),
        )

    def to_sql(self) -> str:
        rows = []

        for row in self.cursor.execute("SELECT class, info, file, pos FROM PII"):
            class_ = escape_str(row[0])
            info_ = escape_str(row[1])
            file_ = escape_str(row[2].replace("./", ""))
            pos_ = row[3]
            rows.append(f"  ('{class_}', '{info_}', '{file_}', {pos_})")

        if not rows:
            return ""

        sql = "INSERT IGNORE INTO PII (class, info, file, pos) VALUES\n"
        return sql + ",\n".join(rows) + ";\n\n"
