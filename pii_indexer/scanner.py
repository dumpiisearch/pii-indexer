from os.path import exists

import textract

from pii_indexer.database import Database
from pii_indexer.patterns import PATTERNS, normalize

FORMAT_WIDTH = 70


class Scanner:
    def __init__(self, database: Database):
        self.database = database

    def scan(self, file_path: str):
        if not exists(file_path):
            print(f"File does not exist: '{file_path}'")
            return

        print(f"Reading file: {file_path} ".ljust(FORMAT_WIDTH, "-"))
        try:
            file_contents = textract.process(file_path).decode("UTF-8")
        except Exception as e:
            print(f"Error reading file '{file_path}': {e}")
            return

        print(f"Scanning file [{len(file_contents)}] ".ljust(FORMAT_WIDTH, "-"))
        for data_type, regex in PATTERNS.items():
            print(f"  {data_type}s ".ljust(FORMAT_WIDTH, "-"))

            for match in regex.finditer(file_contents):
                index = match.start()
                text = file_contents[index : match.end()]
                print(f"    [{index:10}] {text}")
                text = normalize(text, data_type)

                self.database.add_data(
                    data_type, text, file_path, index,
                )
