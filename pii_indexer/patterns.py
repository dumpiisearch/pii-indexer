import re
from typing import Pattern, Dict

PATTERNS: Dict[str, Pattern[str]] = dict(
    EMAIL=re.compile(r"[\d\w][\d\w.+-]+@[\d\w.-]+[\d\w]"),
    HANDLE=re.compile(r"(?<=^|(?<=[^a-zA-Z0-9-_\.]))(@[A-Za-z]+[A-Za-z0-9-_]+)"),
    IPv4=re.compile(
        r"((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))"
    ),
    PHONE=re.compile(r"(\(\d{3}\) ?|\d{3}( ?- ?|.)?)\d{3}( ?- ?|.)?\d{4}"),
    SSN=re.compile(r"(\d{9}|\d{3}-\d{2}-\d{4})"),
)


def normalize(pattern_matched: str, pattern_type: str = None) -> str:
    """
    Clean up a pattern into a standard form so that if the same pattern was
    represented in different places in different forms, a single input will
    hash to the same thing and find all patterns.
    :param pattern_matched: The input string
    :param pattern_type: The type of pattern (should be a key of PATTERNS)
    :return: The normalized string
    """
    pattern_matched = pattern_matched.strip().lower()

    if pattern_type == "HANDLE":
        pattern_matched = pattern_matched.replace("@", "")
    if pattern_type == "PHONE":
        pattern_matched = re.sub(r"[()\-. ]", "", pattern_matched)
    elif pattern_type == "SSN":
        pattern_matched = re.sub(r"[- ]", "", pattern_matched)

    return pattern_matched
